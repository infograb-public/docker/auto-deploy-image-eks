FROM registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v0.16.1

RUN apk --no-cache update

RUN apk --no-cache add python3
RUN pip3 --no-cache-dir install awscli

RUN apk add gettext
